import 'package:Sumedha/models/user.dart';

abstract class AuthBase {
  Future<User> loginUser();
  Future<User> refreshUser();
  Future<User> getUserDataFromToken();
  Future<User> signInSilently();
  Future<void> signOut();
}
