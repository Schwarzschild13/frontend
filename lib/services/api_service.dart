import 'dart:convert';
import 'dart:io';

import 'package:Sumedha/models/category.dart';
import 'package:Sumedha/models/exams.dart' as exam;
import 'package:Sumedha/models/feed.dart';
import 'package:Sumedha/models/materials.dart';
import 'package:Sumedha/models/profile_data.dart';
import 'package:Sumedha/models/progress.dart';
import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/models/subject.dart';
import 'package:Sumedha/models/user.dart';
import 'package:Sumedha/services/network_helper.dart';
import 'package:Sumedha/utilities/constants/api_constants.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart' as path;

class DataSource {
  NetworkUtil _netUtil = NetworkUtil();
  Dio _dio = Dio();
  Future<User> login({@required String username, @required String password}) {
    try {
      return _netUtil.post(APILinkConstants.loginURL, body: {
        AuthStringConstants.username: username,
        AuthStringConstants.password: password
      }).then((dynamic res) {
        if (res != null) {
          return User.fromMap(res);
        } else
          return null;
      });
    } catch (e) {
      return null;
    }
  }

  Future<User> refresh(String token) async {
    return _netUtil.post(APILinkConstants.refreshURL, body: {
      AuthStringConstants.refresh: token,
    }).then((dynamic res) async {
      return User.fromMap(res);
    }).catchError((e) {
      print(e);
      return null;
    });
  }

  Future<dynamic> checkSession(String jwt) async {
    try {
      final response = await _netUtil.read(
          url: APILinkConstants.usersURL,
          headers: <String, String>{'Authorization': 'Bearer ' + jwt});

      return response;
    } catch (e) {
      return e;
    }
  }

  Future<List<Subject>> getAllSubjects(String access) async {
    List<Subject> subjectList;
    var res = await _netUtil
        .get(url: APILinkConstants.subjectsURL, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      subjectList = subjectFromMap(jsonEncode(res));
      return subjectList;
    }
    return null;
  }

  Future<List<Category>> getSubjectCategories({int id, String access}) async {
    List<Category> categoryList;
    var res = await _netUtil.get(
        url: APILinkConstants.subjectsURL +
            id.toString() +
            APILinkConstants.categoriesURL,
        headers: <String, String>{
          'Authorization': 'Bearer ' + access,
        }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      categoryList = categoryFromMap(jsonEncode(res));
      return categoryList;
    }
    return null;
  }

  Future<List<Materials>> getMaterialCategories({int id, String access}) async {
    List<Materials> materialsList;
    var res = await _netUtil.get(
        url: APILinkConstants.materialURL + id.toString(),
        headers: <String, String>{
          'Authorization': 'Bearer ' + access,
        }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      materialsList = materialsFromMap(jsonEncode(res));

      return materialsList;
    }
    return null;
  }

  Future<List<exam.Exam>> getAllExams(String access) async {
    List<exam.Exam> examsList;

    var res = await _netUtil
        .get(url: APILinkConstants.examListURL, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError((e) {
      print(e);
      return null;
    });

    if (res != null) {
      examsList = exam.examFromMap(jsonEncode(res));
      return examsList;
    }
    return null;
  }

  Future<List<ProfileData>> getProfileDatas({@required String access}) async {
    List<ProfileData> profileData;

    var res = await _netUtil
        .get(url: APILinkConstants.profileData, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError(
      (e) {
        print(e);
        return null;
      },
    );
    if (res != null) {
      profileData = profileDatafromMap(jsonEncode(res));
      return profileData;
    }
    return null;
  }

  Future<List<StudentExam>> getStudentExams({@required String access}) async {
    List<StudentExam> studentExamList;
    var res = await _netUtil.get(
        url: APILinkConstants.studentExamListURL,
        headers: <String, String>{
          'Authorization': 'Bearer ' + access,
        }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      studentExamList = studentExamFromMap(jsonEncode(res));
      return studentExamList;
    }
    return null;
  }

  Future<Questions> getQuestions(
      {@required String access, @required String slug}) async {
    Questions questions;
    var res = await _netUtil
        .get(url: APILinkConstants.examsURL + slug, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      questions = questionsFromMap(jsonEncode(res));
      return questions;
    }
    return null;
  }

  Future<String> submitAnswer(
      {@required String access,
      @required int questionID,
      @required String slug,
      @required int answerID}) async {
    try {
      var res = await _dio.put(APILinkConstants.submitAnswerURL + slug,
          data: {"question": questionID, "answer": answerID},
          options: Options(method: 'PUT', headers: <String, String>{
            'Authorization': 'Bearer ' + access,
          }));

      if (res.statusCode == 200) {
        return res.data['message'] ?? 'error';
      } else {
        print(res);
        return 'error';
      }
    } catch (e) {
      print(e);

      return 'error';
    }
  }

  Future<bool> submitTest({
    @required String access,
    @required String slug,
  }) async {
    try {
      var res = await _dio.post(
          APILinkConstants.submitTestURL + slug + APILinkConstants.submitString,
          data: {},
          options: Options(method: 'POST', headers: <String, String>{
            'Authorization': 'Bearer ' + access,
          }));

      if (res.statusCode == 200)
        return true;
      else {
        print(res);
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<List<FeedsList>> getAllFeeds({
    @required String access,
  }) async {
    List<FeedsList> feeds;
    var res = await _netUtil
        .get(url: APILinkConstants.allFeedURL, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      feeds = feedsListFromMap(jsonEncode(res));
      return feeds;
    }
    return null;
  }

  postMaterialRead(
      {@required String access, @required String materialID}) async {
    try {
      await _netUtil.get(
          url: APILinkConstants.materialReadURL + materialID + '/',
          headers: <String, String>{
            'Authorization': 'Bearer ' + access,
          });
    } catch (e) {
      print(e);
      return 400;
    }
  }

  Future<List<Feed>> getFeedForID(
      {@required String access, @required String id}) async {
    List<Feed> feeds;
    var res = await _netUtil.get(
        url: APILinkConstants.getFeedURL +
            id +
            APILinkConstants.getFeedURLSuffix,
        headers: <String, String>{
          'Authorization': 'Bearer ' + access,
        }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      feeds = feedFromMap(jsonEncode(res));
      return feeds;
    }
    return null;
  }

  Future<int> downloadAsByte({
    @required String url,
    @required String fullPath,
  }) async {
    try {
      Response response = await _dio.get(
        url,
        onReceiveProgress: showDownloadProgress,
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
      );
      print(response.statusCode);
      File file = File(fullPath);

      var raf = file.openSync(mode: FileMode.write);

      raf.writeFromSync(response.data);
      await raf.close();

      return response.statusCode;
    } catch (e) {
      print(e);
      return 400;
    }
  }

  void showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }

  Future<ProgressModel> getProgress({
    @required String access,
  }) async {
    var res = await _netUtil
        .get(url: APILinkConstants.getProgressURL, headers: <String, String>{
      'Authorization': 'Bearer ' + access,
    }).catchError((e) {
      print(e);
      return null;
    });
    if (res != null) {
      ProgressModel progress = progressModelFromMap(jsonEncode(res));

      return progress;
    }
    return null;
  }

  Future<int> downloadFile(
      {@required String downloadURL,
      @required fileName,
      @required String access}) async {
    Directory dir = await path.getApplicationDocumentsDirectory();

    String filePath = dir.path;

    await _dio
        .download(
      downloadURL,
      filePath + '/Sumedha/' + fileName,
      options: Options(headers: {
        'Authorization': 'Bearer ' + access,
      }),
    )
        .then((value) {
      return value.statusCode;
    }).catchError((e) {
      print(e);
      return 404;
    });
    return 404;
  }
}
