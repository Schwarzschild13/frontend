import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuestionCard extends StatefulWidget {
  const QuestionCard({
    Key key,
    @required this.questionSet,
  }) : super(key: key);
  final QuestionSet questionSet;

  @override
  _QuestionCardState createState() => _QuestionCardState();
}

class _QuestionCardState extends State<QuestionCard> {
  int selectedAnswer = 0;
  int selectedAnswerID = 0, selectedQuestionID = 0;

  @override
  void initState() {
    selectedAnswer = 0;
    selectedQuestionID = widget.questionSet.id;
    selectedAnswerID = widget.questionSet.answerSet[0].id;
    super.initState();
  }

  @override
  void dispose() {
    selectedAnswer = 0;
    selectedQuestionID = widget.questionSet.id;
    selectedAnswerID = widget.questionSet.answerSet[0].id;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);

    return Consumer<QuestionsProvider>(
        builder: (context, questionProvider, child) {
      return Column(
        children: [
          Card(
            elevation: 1,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.black, width: 1.5),
                borderRadius: BorderRadius.circular(40)),
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            child: Container(
              padding: EdgeInsets.all(4),
              child: Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Text(
                            (questionProvider.currentQuestion + 1).toString() +
                                ') ',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              widget.questionSet.label,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                        ],
                      ),
                    ),
                    RadioListTile(
                      activeColor: ColorConstants.primaryRed,
                      groupValue: questionProvider
                          .markedAnswersList[questionProvider.currentQuestion]
                          .selectedOption,
                      onChanged: (v) {
                        print(questionProvider
                            .markedAnswersList[questionProvider.currentQuestion]
                            .selectedOption);
                        setState(() {
                          questionProvider
                              .markedAnswersList[
                                  questionProvider.currentQuestion]
                              .selectedOption = v;
                          questionProvider.markAnswer(
                              selectedOption: v,
                              questionID: widget.questionSet.id,
                              answerID: widget.questionSet.answerSet[0].id);
                        });
                      },
                      value: 0,
                      title: Text(widget.questionSet.answerSet[0].label),
                    ),
                    RadioListTile(
                      activeColor: ColorConstants.primaryRed,
                      groupValue: questionProvider
                          .markedAnswersList[questionProvider.currentQuestion]
                          .selectedOption,
                      onChanged: (v) {
                        setState(() {
                          questionProvider
                              .markedAnswersList[
                                  questionProvider.currentQuestion]
                              .selectedOption = v;
                          questionProvider.markAnswer(
                              selectedOption: v,
                              questionID: widget.questionSet.id,
                              answerID: widget.questionSet.answerSet[1].id);
                        });
                      },
                      value: 1,
                      title: Text(widget.questionSet.answerSet[1].label),
                    ),
                    RadioListTile(
                      activeColor: ColorConstants.primaryRed,
                      groupValue: questionProvider
                          .markedAnswersList[questionProvider.currentQuestion]
                          .selectedOption,
                      onChanged: (v) {
                        setState(() {
                          questionProvider.markAnswer(
                              selectedOption: v,
                              questionID: widget.questionSet.id,
                              answerID: widget.questionSet.answerSet[2].id);
                          questionProvider
                              .markedAnswersList[
                                  questionProvider.currentQuestion]
                              .selectedOption = v;
                        });
                      },
                      value: 2,
                      title: Text(widget.questionSet.answerSet[2].label),
                    ),
                    RadioListTile(
                      activeColor: ColorConstants.primaryRed,
                      groupValue: questionProvider
                          .markedAnswersList[questionProvider.currentQuestion]
                          .selectedOption,
                      onChanged: (v) {
                        setState(() {
                          questionProvider.markAnswer(
                              questionID: widget.questionSet.id,
                              selectedOption: v,
                              answerID: widget.questionSet.answerSet[3].id);
                          questionProvider
                              .markedAnswersList[
                                  questionProvider.currentQuestion]
                              .selectedOption = v;
                        });
                      },
                      value: 3,
                      title: Text(widget.questionSet.answerSet[3].label),
                    ),
                  ],
                ),
              ),
              constraints: BoxConstraints(
                  minHeight: screenSize.height / 2, minWidth: screenSize.width),
            ),
          ),
        ],
      );
    });
  }
}
