import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuestionBubble extends StatelessWidget {
  const QuestionBubble({
    Key key,
    @required this.questionSet,
    @required this.questionNumber,
  }) : super(key: key);
  final QuestionSet questionSet;
  final int questionNumber;
  @override
  Widget build(BuildContext context) {
    return Consumer<QuestionsProvider>(
      builder: (context, questionProvider, child) {
        Color color = Colors.white;
        questionProvider.markedAnswersList.forEach((answer) {
          if (questionSet.id == answer.questionID) {
            if (answer.answerState == AnswerState.answered) {
              color = Colors.green;
            } else if (answer.answerState == AnswerState.marked) {
              color = Colors.purple;
              if (answer.selectedOption == -1) {
                color = Colors.orange;
              }
            } else if (answer.answerState == AnswerState.skipped) {
              color = Colors.orange;
            } else if (answer.answerState == AnswerState.unAttended) {
              color = Colors.white;
            }
          }
        });
        return GestureDetector(
          onTap: () {
            questionProvider.moveToQuestion(questionNumber);
          },
          child: Card(
            color: Colors.white,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            elevation: 1,
            child: Container(
              padding: EdgeInsets.all(15),
              child: CircleAvatar(
                  backgroundColor: color,
                  child: Text(
                    questionNumber.toString(),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontWeight: FontWeight.bold, color: Colors.black),
                  )),
            ),
          ),
        );
      },
    );
  }
}
