import 'package:Sumedha/UI/screens/dashboard/rating_widget.dart';
import 'package:Sumedha/models/progress.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SmallCard extends StatelessWidget {
  const SmallCard({
    @required this.icon,
    @required this.title,
    @required this.subtitle,
    @required this.color,
  });

  final IconData icon;
  final String title, subtitle;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final screenSize = ScreenSize(context);
    final theme = Provider.of<Themer>(context);
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Container(
          height: screenSize.height / 7.5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20),
                child: FaIcon(
                  icon,
                  size: 30,
                  color: theme.appTheme == AppTheme.Light
                      ? color
                      : Colors.blueAccent,
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(25, 8, 20, 0),
                      child: Text(
                        title,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(25, 5, 20, 10),
                      child: Text(
                        subtitle,
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LargeCard extends StatelessWidget {
  const LargeCard({
    Key key,
    @required this.progress,
  }) : super(key: key);

  final ProgressModel progress;

  @override
  Widget build(BuildContext context) {
    final screenSize = ScreenSize(context);
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
      child: GestureDetector(
        onTap: () async {},
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.black, width: 1.5),
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Container(
            height: screenSize.height / 3.2,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Overall Test Score :',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Rating(
                      rating: progress.score,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        'Tests left to attempt :',
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text('None ! All caught up'),
                    ),
                    Image.asset(
                      'assets/images/done.png',
                      height: 50,
                      width: 50,
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Theme.of(context).primaryColorLight,
                      radius: 40,
                      child: Text(
                        progress.percentage.toString() + '%',
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      'Material \nViewed',
                      maxLines: 2,
                      style: Theme.of(context).textTheme.headline6,
                      softWrap: false,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
