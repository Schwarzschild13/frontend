import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubeViewer extends StatefulWidget {
  final String vidId;
  YoutubeViewer(this.vidId);

  @override
  _YoutubeViewerState createState() => _YoutubeViewerState();
}

class _YoutubeViewerState extends State<YoutubeViewer> {
  @override
  Widget build(BuildContext context) {
    return OrientationList(widget.vidId);
  }
}

class OrientationList extends StatefulWidget {
  final String vidId;
  OrientationList(this.vidId);

  @override
  _OrientationListState createState() => _OrientationListState();
}

class _OrientationListState extends State<OrientationList> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              child: orientation == Orientation.portrait
                  ? Scaffold(
                      drawer: AppDrawer(),
                      appBar: SumedhaAppBar(),
                      body: Player(widget.vidId),
                    )
                  : Scaffold(
                      body: Player(widget.vidId),
                    ));
        },
      ),
    );
  }
}

class Player extends StatefulWidget {
  final String vidIdd;
  Player(this.vidIdd);

  @override
  PlayerState createState() => PlayerState();
}

class PlayerState extends State<Player> {
  @override
  void initState() {
    String vidLink;
    String videoId;
    vidLink = widget.vidIdd;
    videoId = YoutubePlayer.convertUrlToId(vidLink);
    super.initState();

    _controller = YoutubePlayerController(
        initialVideoId: videoId, // id youtube video
        flags: YoutubePlayerFlags(
          autoPlay: true,
          mute: false,
        ));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  YoutubePlayerController _controller;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: YoutubePlayerBuilder(
              player: YoutubePlayer(
                controller: _controller,
                showVideoProgressIndicator: true,
                progressIndicatorColor: Colors.blueAccent,
              ),
       builder: (context, player){
            return Column(
                children: [
         // some widgets
              player,
    //some other widgets
    ],
    );}
            ),
          ),
        ],
      ),
    );
  }
}
