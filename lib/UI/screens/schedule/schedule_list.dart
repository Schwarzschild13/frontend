import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/schedule/schedule_list_widget.dart';
import 'package:flutter/material.dart';

class ScheduleList extends StatefulWidget {
  @override
  _ScheduleListState createState() => _ScheduleListState();
}

class _ScheduleListState extends State<ScheduleList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SumedhaAppBar(),
      body: ScheduleListWidget(),
    );
  }
}
