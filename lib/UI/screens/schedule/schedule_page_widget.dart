// import 'package:Sumedha/UI/screens/schedule/schedule_calendar.dart';
// import 'package:Sumedha/UI/screens/schedule/schedule_list.dart';
// import 'package:flutter/material.dart';
//
// class SchedulePageWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.only(top: 8, bottom: 24, left: 16, right: 16),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: [
//           Card(
//             margin: EdgeInsets.symmetric(vertical: 16),
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(15),
//                 side: BorderSide(width: 2, color: Colors.black)),
//             child: MyCal(),
//           ),
//           RaisedButton(
//             padding: EdgeInsets.symmetric(vertical: 20),
//             onPressed: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => ScheduleList(),
//                 ),
//               );
//             },
//             elevation: 3,
//             color: Theme.of(context).colorScheme.background,
//             shape: RoundedRectangleBorder(
//               side: BorderSide(
//                   color: Theme.of(context).textTheme.headline1.color),
//               borderRadius: BorderRadius.circular(30),
//             ),
//             child: Text('View Schedule'),
//           ),
//         ],
//       ),
//     );
//   }
// }
