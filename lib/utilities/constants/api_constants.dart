class APILinkConstants {
  static const baseURL = "https://hidden-island-68845.herokuapp.com/v1/";
  static const loginURL = baseURL + "token/";
  static const refreshURL = baseURL + "token/refresh/";
  static const usersURL = baseURL + "users/";

  static const String subjectsBaseURL = 'https://api.sumedha.ml/v1';
  static const String subjectsURL = subjectsBaseURL + '/subjects/';
  static const String getProgressURL =
      'https://api.sumedha.ml/v1/student/materials/read/progress/';
  static const String materialReadURL =
      subjectsBaseURL + '/student/materials/read/';
  static const String allFeedURL = subjectsBaseURL + '/feed';
  static const String getFeedURL = allFeedURL + '/category/';
  static const String getFeedURLSuffix = '/entrys';
  static const String categoriesURL = '/category/';
  static const String materialsURL = '/materials/';
  static const String profileData = 'https://api.sumedha.ml/v1/users/';
  static const String materialURL =
      'https://api.sumedha.ml/v1/subjects/category/materials/';

  static const String examListURL = 'https://api.sumedha.ml/v1/student/exams/';
  static const String studentExamListURL =
      'https://api.sumedha.ml/v1/student/exams/list';
  static const String examsURL = 'https://api.sumedha.ml/v1/student/exams/';
  static const String submitTestURL =
      'https://api.sumedha.ml/v1/student/exams/';
  static const String submitString = '/submit/';
  static const String submitAnswerURL =
      'https://api.sumedha.ml/v1/student/exams/response/save/';
}
