import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SubjectConstants {
  static const double _size = 35;

  static const FaIcon mathsIcon = FaIcon(
    FontAwesomeIcons.calculator,
    size: _size,
    color: ColorConstants.deepViolet,
  );
  static const FaIcon chemistryIcon = FaIcon(
    FontAwesomeIcons.flask,
    size: _size,
    color: ColorConstants.deepPink,
  );
  static const FaIcon physicsIcon = FaIcon(
    FontAwesomeIcons.atom,
    size: _size,
    color: ColorConstants.deepOrange,
  );
  static const FaIcon defaultIcon = FaIcon(
    FontAwesomeIcons.book,
    size: _size,
    color: ColorConstants.deepViolet,
  );

//  static FaIcon getSubjectIcon({
//    @required String subject,
//  }) {
//    FaIcon icon = defaultIcon;
//    switch (subject==) {
//      case Subjects.chemistry:
//        icon = chemistryIcon;
//        break;
//      case Subjects.physics:
//        icon = physicsIcon;
//        break;
//      case Subjects.maths:
//        icon = mathsIcon;
//        break;
//      default:
//        icon = defaultIcon;
//    }
//    return icon;
//  }

}
