import 'dart:convert';

import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageHelper {
  StorageHelper();
  final storage = const FlutterSecureStorage();

  Future<void> saveMapData(
      {@required Map<String, dynamic> data, @required String key}) async {
    String encodedMap = jsonEncode(data);
    await storage.write(key: key, value: encodedMap);
  }

  Future<void> saveString(
      {@required String key, @required String value}) async {
    await storage.write(key: key, value: value);
  }

  Future<String> getString(String key) async {
    String result = await storage.read(key: key);
    if (result.isEmpty) return null;
    return result;
  }

  Future<void> saveTokenData(
      {@required String key, @required String keyName}) async {
    await saveString(key: keyName, value: key);
  }

  Future<Map<String, dynamic>> loadMapData(String key) async {
    Map<String, dynamic> decodedMap = Map<String, dynamic>();
    String map = await storage.read(key: key);
    if (map != null) {
      decodedMap = jsonDecode(map);
      return decodedMap;
    }
    return null;
  }

  Future removeUserLoginData() async {
    await storage.delete(key: AuthStringConstants.username);
    await storage.delete(key: AuthStringConstants.password);
    await storage.delete(key: AuthStringConstants.access);
    await storage.delete(key: AuthStringConstants.refresh);
  }

  Future<bool> saveAsPref(
      {@required String key, @required String value}) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    bool result = await sharedPreferences.setString(key, value);
    return result;
  }

  Future<String> getFromPref({@required String key}) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    String result = sharedPreferences.getString(key);
    return result;
  }
}
