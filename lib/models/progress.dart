// To parse this JSON data, do
//
//     final progressModel = progressModelFromMap(jsonString);

import 'dart:convert';

ProgressModel progressModelFromMap(String str) =>
    ProgressModel.fromMap(json.decode(str));

String progressModelToMap(ProgressModel data) => json.encode(data.toMap());

class ProgressModel {
  ProgressModel({
    this.materialCount,
    this.userReadCount,
    this.percentage,
    this.score,
  });

  int materialCount;
  int userReadCount;
  double percentage, score;

  ProgressModel copyWith({
    int materialCount,
    int userReadCount,
    double percentage,
    double score,
  }) =>
      ProgressModel(
        materialCount: materialCount ?? this.materialCount,
        userReadCount: userReadCount ?? this.userReadCount,
        percentage: percentage ?? this.percentage,
        score: score ?? this.score,
      );

  factory ProgressModel.fromMap(Map<String, dynamic> json) {
    double s=-1;
    if(json.containsKey('score'))
      s=json["score"];
    if(s==-1)
      s=0;
    return ProgressModel(
      materialCount: json["material_count"],
      userReadCount: json["user_read_count"],
      percentage: json["percentage"],
      score: s,
    );
  }

  Map<String, dynamic> toMap() => {
        "material_count": materialCount,
        "user_read_count": userReadCount,
        "percentage": percentage,
        "score":score,
      };
}
