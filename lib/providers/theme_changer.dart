import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/material.dart';

class Themer with ChangeNotifier {
  StorageHelper _storageHelper = StorageHelper();
  ThemeData _themeData;
  String themeName;
  AppTheme appTheme;
  String logo = LogoStringConstants.sdsLight;
  Themer(this._themeData);
  get theme => _themeData;
  getTheme() {
    return _themeData;
  }

  setTheme(ThemeData theme) async {
    this._themeData = theme;
    if (theme == BaseTheme.darkTheme) {
      themeName = ThemeStringConstants.dark;
      appTheme = AppTheme.Dark;
      logo = LogoStringConstants.sdsDark;
    } else {
      themeName = ThemeStringConstants.light;
      logo = LogoStringConstants.sdsLight;
      appTheme = AppTheme.Light;
    }
    await _storageHelper.saveAsPref(
        key: ThemeStringConstants.theme, value: themeName);
    notifyListeners();
  }
}
