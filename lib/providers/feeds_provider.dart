import 'package:Sumedha/models/feed.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class FeedsProvider extends ChangeNotifier {
  List<FeedsList> allFeedsList;
  StorageHelper _storageHelper = StorageHelper();
  DataSource _dataSource = DataSource();

  Future<List<FeedsList>> getAllFeeds() async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    allFeedsList = await _dataSource.getAllFeeds(access: access);
    return allFeedsList;
  }

  Future<List<Feed>> getFeedForID(int id) async {
    List<Feed> feed;
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    feed = await _dataSource.getFeedForID(access: access, id: id.toString());
    return feed;
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }
}
