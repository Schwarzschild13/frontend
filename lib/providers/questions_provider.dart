import 'dart:async';

import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class QuestionsProvider extends ChangeNotifier {
  DataSource _dataSource = DataSource();
  Questions questions;
  int currentQuestion = 0;
  int totalQuestions = 0;
  String slug;
  int timerTick = 0;
  DateTime startTime, endTime;
  Duration examDuration, current;
  // Timer _timer;
  bool fetchedQuestions = false, isNewExam = true;
  bool isFirstQuestion = true, isLastQuestion = false;
  bool isExamCompleted = false;
  StorageHelper _storageHelper = StorageHelper();
  List<MarkedAnswers> markedAnswersList = [];
  List<int> markedQuestions = [];

  Future<Questions> getAllQuestions({@required String slug}) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    questions = await _dataSource.getQuestions(access: access, slug: slug);

    totalQuestions = questions.exam.questionSet.length - 1;
    isExamCompleted = false;
    fetchedQuestions = false;
    isFirstQuestion = true;
    isLastQuestion = false;
    this.slug = slug;
    // startTime = DateTime.now();
    // examDuration = Duration(minutes: questions.exam.duration);
    // endTime = startTime.add(examDuration);
    return questions;
  }

  void reFetchQuestions() async {
    fetchedQuestions = false;
    notifyListeners();
  }

  void uncheckAnswer({
    @required int questionID,
    @required int answerID,
  }) {
    markedAnswersList.forEach((answer) {
      if (questionID == answer.questionID) {
        answer.selectedOption = -1;
        answer.answerID = answerID;
        answer.answerState = AnswerState.skipped;
      }
    });

    notifyListeners();
  }

  void startExam(Questions questions) {
    if (!fetchedQuestions) {
      markedAnswersList = [];
      markedQuestions = [];
      questions.exam.questionSet.forEach((question) {
        markedAnswersList.add(MarkedAnswers(
            answerState: AnswerState.unAttended,
            questionID: question.id,
            answerID: question.answerSet[0].id,
            selectedOption: -1));
      });
      totalQuestions = questions.exam.questionSet.length - 1;
      isFirstQuestion = true;
      isLastQuestion = false;
      fetchedQuestions = true;
      isExamCompleted = false;
      startTime = DateTime.now();
      examDuration = Duration(minutes: questions.exam.duration);
      endTime = startTime.add(examDuration);
      current = Duration(seconds: examDuration.inSeconds);
      timerTick = examDuration.inSeconds;
    }
  }

  void moveToQuestion(int questionNumber) {
    if (markedAnswersList[currentQuestion].selectedOption == -1) {
      markedAnswersList[currentQuestion].answerState = AnswerState.skipped;
    }
    currentQuestion = questionNumber - 1;
    if (markedAnswersList[currentQuestion].answerState != AnswerState.answered)
      markedAnswersList[currentQuestion].answerState = AnswerState.marked;
    notifyListeners();
  }

  Future<bool> endExam() async {
    bool isTestSubmitted = false;
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    isTestSubmitted =
        await _dataSource.submitTest(access: access, slug: slug) ?? false;
    markedAnswersList = [];
    markedQuestions = [];
    questions.exam.questionSet.forEach((question) {
      markedAnswersList.add(MarkedAnswers(
          questionID: 1,
          answerID: 0,
          selectedOption: -1,
          answerState: AnswerState.unAttended));
    });
    currentQuestion = 0;
    // isFirstQuestion = true;
    fetchedQuestions = false;
    // isLastQuestion = false;
    totalQuestions = 0;
    slug = '';
    // _timer.cancel();
    notifyListeners();

    isExamCompleted = false;
    return isTestSubmitted;
  }

  markAnswer(
      {@required int questionID, @required int answerID, int selectedOption}) {
    markedAnswersList.forEach((answer) {
      if (questionID == answer.questionID) {
        answer.selectedOption = selectedOption;
        answer.answerID = answerID;
        answer.answerState = AnswerState.marked;
      }
    });

    notifyListeners();
  }

  retryExam() {
    currentQuestion--;
    isLastQuestion = false;
    isExamCompleted = false;
    notifyListeners();
  }

  Future<String> submitCurrentAnswer() async {
    String submissionResponse = 'success';

    if (currentQuestion <= totalQuestions) {
      await getNewAccessToken();
      String access =
          await _storageHelper.getString(AuthStringConstants.access);
      submissionResponse = await _dataSource.submitAnswer(
              slug: slug,
              access: access,
              questionID: markedAnswersList[currentQuestion].questionID,
              answerID: markedAnswersList[currentQuestion].answerID) ??
          'error';
      if (submissionResponse != 'error' || submissionResponse != 'timeout') {
        markedAnswersList[currentQuestion].answerState = AnswerState.answered;
      }
      if (DateTime.now().isAfter(endTime)) {
        isLastQuestion = true;
        isExamCompleted = true;
        currentQuestion = totalQuestions;
        notifyListeners();
        return 'timeout';
      }
      currentQuestion++;
      current = endTime.difference(DateTime.now());
      if (currentQuestion == totalQuestions) {
        isLastQuestion = true;
        isExamCompleted = true;
      }
      notifyListeners();
    }
    return submissionResponse;
  }

  nextQuestion() {
    if (currentQuestion <= totalQuestions) {
      if (markedAnswersList[currentQuestion].answerState !=
              AnswerState.answered &&
          markedAnswersList[currentQuestion].selectedOption > -1)
        markedAnswersList[currentQuestion].answerState = AnswerState.marked;
      else if (markedAnswersList[currentQuestion].selectedOption == -1) {
        markedAnswersList[currentQuestion].answerState = AnswerState.skipped;
      }
      if (DateTime.now().isAfter(endTime)) {
        isLastQuestion = true;
        isExamCompleted = true;
        currentQuestion = totalQuestions;
      }

      currentQuestion++;
      if (currentQuestion == totalQuestions - 1) {
        if (markedAnswersList[currentQuestion].answerState !=
                AnswerState.answered &&
            markedAnswersList[currentQuestion].selectedOption > -1)
          markedAnswersList[currentQuestion].answerState = AnswerState.marked;
        else if (markedAnswersList[currentQuestion].selectedOption == -1) {
          markedAnswersList[currentQuestion].answerState = AnswerState.skipped;
        }
      }
      current = endTime.difference(DateTime.now());
      if (currentQuestion == totalQuestions) {
        isLastQuestion = true;
        isExamCompleted = true;
      }
    }
    notifyListeners();
  }

  void prevQuestion() {
    if (currentQuestion == 0) {
      isFirstQuestion = true;
      isLastQuestion = false;
    }
    if (currentQuestion > 0) {
      if (markedAnswersList[currentQuestion].answerState !=
              AnswerState.answered &&
          markedAnswersList[currentQuestion].selectedOption > -1)
        markedAnswersList[currentQuestion].answerState = AnswerState.marked;
      else if (markedAnswersList[currentQuestion].selectedOption == -1) {
        markedAnswersList[currentQuestion].answerState = AnswerState.skipped;
      }
      currentQuestion--;
      if (markedAnswersList[currentQuestion].answerState !=
              AnswerState.answered &&
          markedAnswersList[currentQuestion].selectedOption > -1)
        markedAnswersList[currentQuestion].answerState = AnswerState.marked;
      else if (markedAnswersList[currentQuestion].selectedOption == -1) {
        markedAnswersList[currentQuestion].answerState = AnswerState.skipped;
      }
      isLastQuestion = false;
    } else {
      isLastQuestion = false;
      isFirstQuestion = true;
    }
    notifyListeners();
  }

  QuestionSet getQuestion(int questionNumber) {
    currentQuestion = questionNumber;
    return questions.exam.questionSet[questionNumber];
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }

  void gotoLastQuestion(){
    currentQuestion = totalQuestions+1;
    notifyListeners();
  }

}


class MarkedAnswers {
  int questionID, answerID, selectedOption;
  AnswerState answerState;
  MarkedAnswers(
      {this.answerID, this.questionID, this.selectedOption, this.answerState});
  @override
  String toString() {
    print(' Question id: ' +
        questionID.toString() +
        '\nAnswer Id : ' +
        answerID.toString() +
        '\n Selected option : ' +
        selectedOption.toString());
    return super.toString();
  }
}

