import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/UI/screens/auth/login_screen.dart';
import 'package:Sumedha/UI/screens/home/home_screen.dart';
import 'package:Sumedha/UI/screens/splash/splash_screen.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/bottom_navbar_provider.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:Sumedha/providers/feeds_provider.dart';
import 'package:Sumedha/providers/profile_provider.dart';
import 'package:Sumedha/providers/progress_provider.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AuthService _auth = AuthService();
  Themer _themer = Themer(BaseTheme.lightTheme);
  String theme;

  @override
  void initState() {
    try {
      SharedPreferences.getInstance().then((prefs) {
        if (prefs.getString(ThemeStringConstants.theme) ==
            ThemeStringConstants.dark)
          _themer.setTheme(BaseTheme.darkTheme);
        else if (prefs.getString(ThemeStringConstants.theme) ==
            ThemeStringConstants.light)
          _themer.setTheme(BaseTheme.lightTheme);
        else
          _themer.setTheme(BaseTheme.lightTheme);
      });
    } catch (e) {
      print("Error Loading Theme: $e");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<Themer>(
            create: (_) => _themer,
          ),
          ChangeNotifierProvider<SubjectProvider>(
            create: (_) => SubjectProvider(),
          ),
          ChangeNotifierProvider<ExamProvider>(
            create: (_) => ExamProvider(),
          ),
          ChangeNotifierProvider<QuestionsProvider>(
            create: (_) => QuestionsProvider(),
          ),
          ChangeNotifierProvider<FeedsProvider>(
            create: (_) => FeedsProvider(),
          ),
          ChangeNotifierProvider<ProgressProvider>(
            create: (_) => ProgressProvider(),
          ),
          ChangeNotifierProvider<ProfileProvider>(
            create: (_) => ProfileProvider(),
          ),
          ChangeNotifierProvider<AuthService>(create: (_) => _auth),
          ChangeNotifierProvider<BottomNavigationBarProvider>(
            create: (_) => BottomNavigationBarProvider(),
          ),
        ],
        child: Consumer<Themer>(
          builder: (context, theme, child) => MaterialApp(
            themeMode: ThemeMode.system,
            debugShowCheckedModeBanner: false,
            title: 'Sumedha',
            routes: {
              '/': (context) => SplashScreen(
                    auth: _auth,
                  ),
              '/home': (context) => HomePage(),
              '/login': (context) => LoginScreen(),
            },
            initialRoute: '/',
            theme: theme.getTheme(),
          ),
        ));
  }
}
